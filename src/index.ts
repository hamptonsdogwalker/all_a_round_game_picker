import { MAX_LENGTH, GAMES } from "./games";

function random_games() {
	let games_copy = [...GAMES];
	let current_time = 0;

	const container = document.getElementById("games");
	container!.textContent = "";

	while (!(current_time >= MAX_LENGTH)) {
		console.log("here", games_copy)
		const game_index = Math.floor(Math.random() * games_copy.length);
		const { name, time } = games_copy[game_index];

		const li_element = document.createElement("li");
		li_element.textContent = name;
		container!.appendChild(li_element);

		games_copy.splice(game_index, 1);
		current_time += time;
	}
}

function main() {
	const button = document.getElementById("button");
	button!.onclick = random_games;
}

main();
export const MAX_LENGTH = 120;
export const GAMES = [
	{ name: "Risk of Rain 2", time: 30 },
	{ name: "Vagante", time: 5 },
	{ name: "Risk of Rain", time: 30 },
	{ name: "BPM", time: 15 },
	{ name: "Fall Guys", time: 10 },
	{ name: "Hades", time: 30 },
	{ name: "Binding of Isaac", time: 25 },
	{ name: "Marbles", time: 15 },
	{ name: "FTL", time: 10 },
	{ name: "Choice Chamber", time: 20 },
	{ name: "Spellbreak", time: 25 },
	{ name: "Fortnight", time: 25 }
];
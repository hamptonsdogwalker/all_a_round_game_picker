const path = require('path');

module.exports = {
    entry: "./src/index.ts",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, 'public')
    },
    resolve: {
        extensions: [".ts"]
    },
    module: {
        rules: [{ test: /\.ts$/, loader: "ts-loader"}]
    },
}